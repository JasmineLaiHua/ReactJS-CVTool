/*We Create server.js file on root directory for writing nodejs apis for 
our Create ,Insert,Delete,Update. Give port number for application and run on server.
In this file we write the code for all required dependancies and create schema of 
our database collection document and write code for api's for performing operation.*/

//import dependencies
let express = require('express');
let mongoose = require('mongoose');
let bodyParser = require('body-parser');
let Candidate = require('./src/model/Candidates');


//create instances
let app = express();
let router = express.Router();

//set port
let port = process.env.PORT || 3001;

//db config
mongoose.connect("mongodb://admin:ng0cl%40i@ds117691.mlab.com:17691/resumemanagement");
let db = mongoose.connection;
db.on('err', console.error.bind(console, 'MongoDB connection error:'));

db.once('open', function(){
    console.log("Alive");
});


//configure the APi to use bodyParser and look for JSON data in the body
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

//To prevent errors from Cross Origin Resource Sharing, we will set our headers to allow CORS with middleware like so:
app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');
  
    //and remove cacheing so we get the most recent comments
    res.setHeader('Cache-Control', 'no-cache');
    next();
});

//get document
app.get('/:userId', function(req, res){
    let userId = req.params.userId;
    Candidate.find({'userId': userId},function(err, document){
        if(err) console.log(err);
        else{
            res.json(document);
        }
    })
})

app.listen(port);
console.log('Magic happens on port ' + port);


