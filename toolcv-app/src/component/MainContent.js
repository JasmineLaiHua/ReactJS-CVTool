import React, { Component } from 'react';
import { connect } from "react-redux";
import { handleEditButton,handleEditValue } from '../actions/action';
import '../css/MainContent.css';


class MainContent extends Component {
    constructor(props) {
        super(props);
    }

    handleEnter(e){
        if(e.key === "Enter"){
            console.log("fd");
            this.props.handleEditButton;
        }
        else{
            return null;
        }
    }

    render() {
        return (
            <div>
                {this.props.isEdited ? (
                    <h1 id="name" className="maincontent__header">
                        <input type="text" value={this.props.candidate.name} onChange={this.props.handleEditValue}
                             onKeyPress={(e)=>{this.handleEnter(e)}}/>
                        <button onClick={this.props.handleEditButton}>Done</button>
                    </h1>
                ) : (
                        <h1 id="name" className="maincontent__header">{this.props.candidate.name}
                            <i name="edit" className="far fa-edit" onClick={this.props.handleEditButton}></i>
                        </h1>
                )}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        candidate: state.candidate,
        isEdited: state.isEdited
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleEditButton: () => dispatch(handleEditButton()),
        handleEditValue: (event) => dispatch(handleEditValue(event.target.value))
    };

};


export default connect(mapStateToProps, mapDispatchToProps)(MainContent);

