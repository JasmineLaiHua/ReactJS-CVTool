import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';
import {updateInputValue} from '../actions/action';
import {connect} from "react-redux";
import axios from 'axios';

class Home extends Component {
    constructor(props) {
        super(props);
    }

    handleOnClick() {
        this.props.history.push({
            pathname: `/mainpage/${this.props.userId}`,
            state: {userId: this.props.userId}
        });
    }

    render() {
        return (
            <form>
                <div className="form-group">
                    <label>User ID</label>
                    <input type="email" className="form-control" id="exampleInputEmail1" placeholder="Enter user id"
                        onChange={this.props.handleOnChange} />
                </div>

                <button type="submit" className="btn btn-primary" >New</button>
                <button type="button" className="btn btn-success" onClick={() => { this.handleOnClick() }}>Edit</button>
            </form>

        )
    }

}

const mapStateToProps = (state) => {
    return {
        userId: state.userId
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        handleOnChange: (event)=> dispatch(updateInputValue(event.target.value))
        // handleOnClick: () => dispatch()
    };

};

export default connect(mapStateToProps, mapDispatchToProps)(Home);