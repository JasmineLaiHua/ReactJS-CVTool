import React from 'react';
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import MainContent from './MainContent';
import Home from './Home';
import {fetchCandidates} from '../actions/action';
import {connect} from "react-redux";

import '../css/App.css';
import '../../node_modules/bootstrap/dist/css/bootstrap.min.css';


class MainPage extends React.Component {
    constructor(props) {
        super(props);
        // console.log(this.props.location.state.userId);
    }

    componentDidMount() {
        axios.get(`http://localhost:3001/${this.props.location.state.userId}`,{params: {userId: this.props.location.state.userId}})
        .then(res => {
            let index = res.data.findIndex((x) => x.userId == this.props.location.state.userId);
            this.props.fetchCandidates(res.data[index]);
        })
        .catch(err => {
            console.log(err);
        });
    }

    render() {
        return (
            //<h1>Mainpage</h1>
            <div className="container-fluid">

                <div className="row row--mod">
                    <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12"></div>
                </div>

                <div className="row">
                    <div className="col-xs-8 col-sm-8 col-md-8 col-lg-9 maincontent">
                        <MainContent className="maincontent"/>
                    </div>
                </div>   
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        candidate: state.candidate
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        fetchCandidates: (candidate)=> dispatch(fetchCandidates(candidate))
    };

};

export default connect(mapStateToProps, mapDispatchToProps)(MainPage);