export const fetchCandidates = (candidate) =>{
    return {
        type: "FETCH_CANDIDATES",
        candidate: candidate
    }
}


export const updateInputValue = (userId) =>{
    return {
        type: "UPDATE_INPUT_VALUE",
        userId: userId
    }
}

export const handleEditButton = () =>{
    return {
        type: "HANDLE_EDIT_BUTTON"
    }
}

export const handleEditValue = (value) =>{
    return {
        type: "HANDLE_EDIT_VALUE",
        value: value   
    }
}



