import React from 'react';
import ReactDOM from "react-dom";
import { Switch, Route } from 'react-router-dom';
import axios from 'axios';
import MainPage from './component/MainPage';
import Home from './component/Home';

//import './css/App.css';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';

const App = () => {
    return (
        <div>
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/mainpage' component={MainPage} />
            </Switch>
        </div>
    );
}

export default App;