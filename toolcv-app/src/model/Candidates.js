//import dependency
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//create new instance of the mongoose.Schema. Schema: the shape of your db entries
var candidatesSchema = new Schema({
    name: String
});

//export our module to use in server.js
module.exports =  mongoose.model('Candidate', candidatesSchema);
