
import React from "react";
import ReactDOM from "react-dom";
import { HashRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import App from './App';
import 'bootstrap';

const INIT_STORE = {
  candidate: {},
  userId: 0,
  isEdited: false
};

const reducer = (prevState = INIT_STORE, action) => {
  switch (action.type) {
    case "FETCH_CANDIDATES":
      return Object.assign({}, prevState, {

        candidate: action.candidate
      })
      break;
    case "UPDATE_INPUT_VALUE":
      return {
        ...prevState,
        userId: action.userId
      }
      break;
    case "HANDLE_EDIT_BUTTON":
      console.log("gfgf");
      return {
        ...prevState,
        isEdited: !prevState.isEdited
      }
      break;
    case "HANDLE_EDIT_VALUE":
      return {
        ...prevState,
        candidate: {
          ...prevState.candidate,
          name: action.value
        }
      }
      break;
    default:
      return prevState;
  };
};



const store = createStore(reducer);

//console.log(store.getState())
const unSubscribe = store.subscribe(() => {
  console.log(store.getState())
})

// render(what we want to render, where we want to output)
ReactDOM.render((
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>
), document.getElementById('root'));